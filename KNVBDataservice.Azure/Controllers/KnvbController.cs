﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.OutputCache.V2;

namespace KNVBDataservice.Azure.Controllers
{
    public class KnvbController : ApiController
    {

        [Route("api/knvb/ping")]
        [HttpGet]
        public HttpResponseMessage Ping()
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent("Pong")
            };
        }

        [Route("api/knvb/init")]
        [HttpGet]
        [CacheOutput(ClientTimeSpan = 600, ServerTimeSpan = 600)]
        public async Task<HttpResponseMessage> Init(string key, string path)
        {
            Hatch.Hatch hatch = new Hatch.Hatch(key, path);
            string sessionid = await hatch.InitAsync();
            return new HttpResponseMessage() { Content = new StringContent(sessionid) };
        }

        #region competition calls
        [Route("api/knvb/competitions")]
        [HttpGet]
        [CacheOutput(ClientTimeSpan = 600, ServerTimeSpan = 600)]
        public async Task<HttpResponseMessage> GetCompetitions(string key, string path, string sessionid)
        {
            Hatch.Hatch hatch = new Hatch.Hatch(key, path);
            var content = await hatch.GetCompetitionsAsync(sessionid);
            return new HttpResponseMessage() { Content = new StringContent(content) };
        }

        [Route("api/knvb/competitionranking")]
        [HttpGet]
        [CacheOutput(ClientTimeSpan = 600, ServerTimeSpan = 600)]
        public async Task<HttpResponseMessage> GetCompetitionRanking(string key, string path, string sessionid, string teamid, string district, string compid, string classid, string pouleid, string querystring)
        {
            Hatch.Hatch hatch = new Hatch.Hatch(key, path);
            var content = await hatch.GetCompetitionRankingAsync(sessionid, teamid, district, compid, classid, pouleid, querystring);
            return new HttpResponseMessage() { Content = new StringContent(content) };
        }

        [Route("api/knvb/competitionschedule")]
        [HttpGet]
        [CacheOutput(ClientTimeSpan = 600, ServerTimeSpan = 600)]
        public async Task<HttpResponseMessage> GetCompetitionSchedule(string key, string path, string sessionid, string teamid, string district, string compid, string classid, string pouleid, string querystring)
        {
            Hatch.Hatch hatch = new Hatch.Hatch(key, path);
            var content = await hatch.GetCompetitionScheduleAsync(sessionid, teamid, district, compid, classid, pouleid, querystring);
            return new HttpResponseMessage() { Content = new StringContent(content) };
        }

        [Route("api/knvb/competitionresults")]
        [HttpGet]
        [CacheOutput(ClientTimeSpan = 600, ServerTimeSpan = 600)]
        public async Task<HttpResponseMessage> GetCompetitionResults(string key, string path, string sessionid, string teamid, string district, string compid, string classid, string pouleid, string querystring)
        {
            Hatch.Hatch hatch = new Hatch.Hatch(key, path);
            var content = await hatch.GetCompetitionResultsAsync(sessionid, teamid, district, compid, classid, pouleid, querystring);
            return new HttpResponseMessage() { Content = new StringContent(content) };
        }

        #endregion

        #region match calls
        [Route("api/knvb/matches")]
        [HttpGet]
        [CacheOutput(ClientTimeSpan = 600, ServerTimeSpan = 1800)]
        public async Task<HttpResponseMessage> GetMatches(string key, string path, string sessionid, string querystring)
        {
            Hatch.Hatch hatch = new Hatch.Hatch(key, path);
            var content = await hatch.GetMatchesAsync(sessionid, querystring);
            return new HttpResponseMessage() { Content = new StringContent(content) };

        }
        #endregion

        #region team calls
        [Route("api/knvb/teams")]
        [HttpGet]
        [CacheOutput(ClientTimeSpan = 600, ServerTimeSpan = 1800)]
        public async Task<HttpResponseMessage> GetTeams(string key, string path, string sessionid)
        {
            Hatch.Hatch hatch = new Hatch.Hatch(key, path);
            var content = await hatch.GetTeamsAsync(sessionid);
            return new HttpResponseMessage() { Content = new StringContent(content) };
        }

        [Route("api/knvb/team")]
        [HttpGet]
        [CacheOutput(ClientTimeSpan = 600, ServerTimeSpan = 1800)]
        public async Task<HttpResponseMessage> GetTeam(string key, string path, string sessionid, string teamid)
        {
            Hatch.Hatch hatch = new Hatch.Hatch(key, path);
            var content = await hatch.GetTeamAsync(sessionid, teamid);
            return new HttpResponseMessage() { Content = new StringContent(content) };
        }

        [Route("api/knvb/teamranking")]
        [HttpGet]
        [CacheOutput(ClientTimeSpan = 600, ServerTimeSpan = 1800)]
        public async Task<HttpResponseMessage> GetTeamRanking(string key, string path, string sessionid, string teamid, string querystring)
        {
            Hatch.Hatch hatch = new Hatch.Hatch(key, path);
            var content = await hatch.GetTeamRankingAsync(sessionid, teamid, querystring);
            return new HttpResponseMessage() { Content = new StringContent(content) };
        }

        [Route("api/knvb/teamschedule")]
        [HttpGet]
        [CacheOutput(ClientTimeSpan = 600, ServerTimeSpan = 1800)]
        public async Task<HttpResponseMessage> GetTeamSchedule(string key, string path, string sessionid, string teamid, string querystring)
        {
            Hatch.Hatch hatch = new Hatch.Hatch(key, path);
            var content = await hatch.GetTeamScheduleAsync(sessionid, teamid, querystring);
            return new HttpResponseMessage() { Content = new StringContent(content) };
        }

        [Route("api/knvb/teamresults")]
        [HttpGet]
        [CacheOutput(ClientTimeSpan = 600, ServerTimeSpan = 1800)]
        public async Task<HttpResponseMessage> GetTeamResults(string key, string path, string sessionid, string teamid, string querystring)
        {
            Hatch.Hatch hatch = new Hatch.Hatch(key, path);
            var content = await hatch.GetTeamResultsAsync(sessionid, teamid, querystring);
            return new HttpResponseMessage() { Content = new StringContent(content) };
        }
        #endregion
    }
}
