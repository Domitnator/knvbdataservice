﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.OutputCache.V2;

namespace KNVBDataservice.Azure.Controllers
{
    public class SportlinkController : ApiController
    {

        [Route("api/sportlink/ping")]
        [HttpGet]
        public HttpResponseMessage Ping()
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent("Pong")
            };
        }


        [Route("api/sportlink/call")]
        [HttpGet]
        [CacheOutput(ClientTimeSpan = 600, ServerTimeSpan = 1800)]
        public async Task<HttpResponseMessage> Init(string querystring)
        {
            SportlinkData.Hatch.Hatch hatch = new SportlinkData.Hatch.Hatch();
            string content = await hatch.DoCallAsync(querystring);
            return new HttpResponseMessage() { Content = new StringContent(content) };
        }
    }
}
