﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace KNVBDataservice.Tests
{
    [TestClass]
    public class KnvbDataserviceHatchTests
    {
        Hatch.Hatch hatch = null;
        string sessionid = String.Empty;
        const string api_key = "pe5vFJfJsPeUYTk";
        const string api_path = "fceibergen";

        [TestInitialize]
        public void TestInitialize()
        {
            hatch = new Hatch.Hatch(api_key, api_path);
            this.sessionid = hatch.Init();
        }

        [TestMethod]
        public async Task HatchCanInitializeAsync()
        {
            Hatch.Hatch hatch = new Hatch.Hatch(api_key, api_path);

            var sessionid = await hatch.InitAsync();

            Assert.IsFalse(String.IsNullOrEmpty(sessionid));
        }


        [TestMethod]
        public async Task HatchReturnsListOfTeamsAsync()
        {
            var downloadresponse = await hatch.GetTeamsAsync(sessionid);

            Assert.IsFalse(String.IsNullOrEmpty(downloadresponse));
        }

        [TestMethod]
        public async Task HatchReturnsListOfCompetitionsAsync()
        {
            var downloadresponse = await hatch.GetCompetitionsAsync(sessionid);

            Assert.IsFalse(String.IsNullOrEmpty(downloadresponse));
        }

        [TestMethod]
        public async Task HatchReturnsListOfMatchesAsync()
        {
            var downloadresponse = await hatch.GetMatchesAsync(sessionid, "&weeknummer=10");

            Assert.IsFalse(String.IsNullOrEmpty(downloadresponse));
        }
    }
}
