﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KNVBDataservice.Tests
{
    [TestClass]
    public class SportlinkHatchTests
    {
        [TestMethod]
        public void HatchReturnsListOfMatches()
        {
            var downloadresponse = new SportlinkData.Hatch.Hatch().DoCall("programma?client_id=MQoYWlAwpa&gebruiklokaleteamgegevens=NEE&aantaldagen=7&weekoffset=0&eigenwedstrijden=JA&thuis=JA&uit=JA");

            Assert.IsFalse(String.IsNullOrEmpty(downloadresponse));
        }
    }
}
