﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SportlinkData.Hatch
{
    public class Hatch
    {
        private const string ApiDomain = "data.sportlink.com";
        //Example: https://data.sportlink.com/programma?client_id=MQoYWlAwpa&gebruiklokaleteamgegevens=NEE&aantaldagen=7&weekoffset=0&eigenwedstrijden=JA&thuis=JA&uit=JA


        private WebClient CreateWebClient()
        {
            #region body
            return new WebClient();
            #endregion
        }

        public Task<string> DoCallAsync(string querystring)
        {
            #region body
            string hash = String.Empty;

            string requesturi = String.Format("https://{0}/{1}", ApiDomain, querystring);

            using (WebClient client = CreateWebClient())
            {
                return client.DownloadStringTaskAsync(requesturi);
            }

            #endregion
        }

        public string DoCall(string querystring)
        {
            #region body
            string hash = String.Empty;

            string requesturi = String.Format("https://{0}/{1}", ApiDomain, querystring);

            using (WebClient client = CreateWebClient())
            {
                return client.DownloadString(requesturi);
            }

            #endregion
        }
    }
}
