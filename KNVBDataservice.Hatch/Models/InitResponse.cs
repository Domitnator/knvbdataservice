﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KNVBDataservice.Hatch.Models
{
    public class InitResponse
    {
        public int errorcode { get; set; }
        public string message { get; set; }
        public List<InitResponseList> List { get; set; }
    }
}
