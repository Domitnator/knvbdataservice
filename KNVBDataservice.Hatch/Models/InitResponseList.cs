﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KNVBDataservice.Hatch.Models
{
    public class InitResponseList
    {
        public string PHPSESSID { get; set; }
        public string clubnaam { get; set; }
        public string apiversion { get; set; }
        public string changed { get; set; }
        public string changelog { get; set; }
        public string logo { get; set; }
    }
}
