﻿using KNVBDataservice.Hatch.Helpers;
using KNVBDataservice.Hatch.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace KNVBDataservice.Hatch
{
    public partial class Hatch
    {
        private const string ApiDomain = "api.knvbdataservice.nl";

        #region properties
        public string apiurl
        { get; set; }

        public string knvbdataservice_apisleutel
        { get; set; }

        public string knvbdataservice_pathname
        { get; set; }

        #endregion

        #region constructor
        public Hatch(string knvbdataservice_apisleutel, string knvbdataservice_pathname)
        {
            this.knvbdataservice_apisleutel = knvbdataservice_apisleutel;
            this.knvbdataservice_pathname = knvbdataservice_pathname;
        }
        #endregion

        #region init
        private WebClient CreateWebClient()
        {
            #region body
            WebClient wc = new WebClient();
            wc.Headers.Add("HTTP_X_APIKEY", this.knvbdataservice_apisleutel);
            return wc;
            #endregion
        }

        /// <summary>
        /// Will do the initialise call
        /// </summary>
        /// <param name="knvbdataservice_apisleutel">The api-key</param>
        /// <param name="knvbdataservice_pathname">The api-pathname</param>
        /// <returns>a session-id</returns>
        public async Task<string> InitAsync()
        {
            #region body
            this.apiurl = apiurl;
            this.knvbdataservice_apisleutel = knvbdataservice_apisleutel;
            this.knvbdataservice_pathname = knvbdataservice_pathname;

            string requesturi = String.Format("http://{0}/api/initialisatie/{1}", ApiDomain, knvbdataservice_pathname);

            using (WebClient client = CreateWebClient())
            {
                //client.DownloadStringTaskAsync()
                var response = await client.DownloadStringTaskAsync(requesturi);

                InitResponse responseObj = JsonConvert.DeserializeObject<InitResponse>(response);

                if (responseObj != null && responseObj.List != null)
                {
                    return responseObj.List[0].PHPSESSID;
                }
            }

            return string.Empty;
            #endregion
        }

        /// <summary>
        /// Will do the initialise call
        /// </summary>
        /// <param name="knvbdataservice_apisleutel">The api-key</param>
        /// <param name="knvbdataservice_pathname">The api-pathname</param>
        /// <returns>a session-id</returns>
        public string Init()
        {
            #region body
            this.apiurl = apiurl;
            this.knvbdataservice_apisleutel = knvbdataservice_apisleutel;
            this.knvbdataservice_pathname = knvbdataservice_pathname;

            string requesturi = String.Format("http://{0}/api/initialisatie/{1}", ApiDomain, knvbdataservice_pathname);

            using (WebClient client = CreateWebClient())
            {
                //client.DownloadStringTaskAsync()
                var response = client.DownloadString(requesturi);

                InitResponse responseObj = JsonConvert.DeserializeObject<InitResponse>(response);

                if (responseObj != null && responseObj.List != null)
                {
                    return responseObj.List[0].PHPSESSID;
                }
            }

            return string.Empty;
            #endregion
        }
        #endregion

        #region team calls
        /// <summary>
        /// Will retrieve a list of teams
        /// </summary>
        /// <param name="sessionid">The session key (can be retrieved by calling the init function)</param>
        /// <returns>A json string containing a list of teams</returns>
        public Task<string> GetTeamsAsync(string sessionid)
        {
            #region body
            string hash = String.Empty;

            if (!String.IsNullOrEmpty(sessionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, "/teams", sessionid)).ToLower();
            }

            string requesturi = String.Format("http://{0}/api/teams?PHPSESSID={1}&hash={2}", ApiDomain, sessionid, hash);

            using (WebClient client = CreateWebClient())
            {
               return client.DownloadStringTaskAsync(requesturi);
            }

            #endregion
        }

        /// <summary>
        /// Will retrieve a list of teams
        /// </summary>
        /// <param name="sessionid">The session key (can be retrieved by calling the init function)</param>
        /// <returns>A json string containing a list of teams</returns>
        public string GetTeams(string sessionid)
        {
            #region body
            string hash = String.Empty;

            if (!String.IsNullOrEmpty(sessionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, "/teams", sessionid)).ToLower();
            }

            string requesturi = String.Format("http://{0}/api/teams?PHPSESSID={1}&hash={2}", ApiDomain, sessionid, hash);

            using (WebClient client = CreateWebClient())
            {
                return client.DownloadString(requesturi);
            }

            #endregion
        }


        /// <summary>
        /// Will retrieve a team
        /// </summary>
        /// <param name="sessionid">The session key (can be retrieved by calling the init function)</param>
        /// <returns>A json string containing a team</returns>
        public string GetTeam(string sessionid, string teamid)
        {
            #region body
            string hash = String.Empty;

            if (!String.IsNullOrEmpty(sessionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, string.Format("/teams/{0}", teamid), sessionid)).ToLower();
            }

            string requesturi = String.Format("http://api.knvbdataservice.nl/api/teams/{2}?PHPSESSID={0}&hash={1}", sessionid, hash, teamid);

            using (WebClient client = CreateWebClient())
            {
                return client.DownloadString(requesturi);

            }

            #endregion
        }

        /// <summary>
        /// Will retrieve a team
        /// </summary>
        /// <param name="sessionid">The session key (can be retrieved by calling the init function)</param>
        /// <returns>A json string containing a team</returns>
        public Task<string> GetTeamAsync(string sessionid, string teamid)
        {
            #region body
            string hash = String.Empty;

            if (!String.IsNullOrEmpty(sessionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, string.Format("/teams/{0}", teamid), sessionid)).ToLower();
            }

            string requesturi = String.Format("http://api.knvbdataservice.nl/api/teams/{2}?PHPSESSID={0}&hash={1}", sessionid, hash, teamid);

            using (WebClient client = CreateWebClient())
            {
                return client.DownloadStringTaskAsync(requesturi);

            }

            #endregion
        }

        public string GetTeamRanking(string sesionid, string teamid, string querystring)
        {
            #region body
            string hash = String.Empty;

            if (!String.IsNullOrEmpty(sesionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, String.Format("/teams/{0}/ranking", teamid), sesionid)).ToLower();
            }

            string requesturi = String.Format("http://api.knvbdataservice.nl/api/teams/{2}/ranking?PHPSESSID={0}&hash={1}", sesionid, hash, teamid, querystring);

            using (WebClient client = CreateWebClient())
            {
                return client.DownloadString(requesturi);
            }
            #endregion
        }

        public Task<string> GetTeamRankingAsync(string sesionid, string teamid, string querystring)
        {
            #region body
            string hash = String.Empty;

            if (!String.IsNullOrEmpty(sesionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, String.Format("/teams/{0}/ranking", teamid), sesionid)).ToLower();
            }

            string requesturi = String.Format("http://api.knvbdataservice.nl/api/teams/{2}/ranking?PHPSESSID={0}&hash={1}", sesionid, hash, teamid, querystring);

            using (WebClient client = CreateWebClient())
            {
                return client.DownloadStringTaskAsync(requesturi);
            }
            #endregion
        }

        public string GetTeamSchedule(string sessionid, string teamid, string querystring)
        {
            #region body
            string hash = String.Empty;

            if (!String.IsNullOrEmpty(sessionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, String.Format("/teams/{0}/schedule", teamid), sessionid)).ToLower();
            }

            string requesturi = String.Format("http://api.knvbdataservice.nl/api/teams/{2}/schedule?PHPSESSID={0}&hash={1}{3}", sessionid, hash, teamid, querystring);

            using (WebClient client = CreateWebClient())
            {
                return client.DownloadString(requesturi);
            } 
            #endregion
        }

        public Task<string> GetTeamScheduleAsync(string sessionid, string teamid, string querystring)
        {
            #region body
            string hash = String.Empty;

            if (!String.IsNullOrEmpty(sessionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, String.Format("/teams/{0}/schedule", teamid), sessionid)).ToLower();
            }

            string requesturi = String.Format("http://api.knvbdataservice.nl/api/teams/{2}/schedule?PHPSESSID={0}&hash={1}{3}", sessionid, hash, teamid, querystring);

            using (WebClient client = CreateWebClient())
            {
                return client.DownloadStringTaskAsync(requesturi);
            }
            #endregion
        }

        public string GetTeamResults(string sessionid,string teamid, string querystring)
        {
            #region body
            string hash = String.Empty;

            if (!String.IsNullOrEmpty(sessionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, String.Format("/teams/{0}/results", teamid), sessionid)).ToLower();
            }

            string requesturi = String.Format("http://api.knvbdataservice.nl/api/teams/{2}/results?PHPSESSID={0}&hash={1}{3}", sessionid, hash, teamid, querystring);


            using (WebClient client = CreateWebClient())
            {
                return client.DownloadString(requesturi);
            }
            #endregion
        }

        public Task<string> GetTeamResultsAsync(string sessionid, string teamid, string querystring)
        {
            #region body
            string hash = String.Empty;

            if (!String.IsNullOrEmpty(sessionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, String.Format("/teams/{0}/results", teamid), sessionid)).ToLower();
            }

            string requesturi = String.Format("http://api.knvbdataservice.nl/api/teams/{2}/results?PHPSESSID={0}&hash={1}{3}", sessionid, hash, teamid, querystring);


            using (WebClient client = CreateWebClient())
            {
                return client.DownloadStringTaskAsync(requesturi);
            }
            #endregion
        }
        #endregion

        #region competition calls
        /// <summary>
        /// Will retrieve a list of competitions
        /// </summary>
        /// <param name="sessionid">The session key (can be retrieved by calling the init function)</param>
        /// <returns>A json string containing a list of competitions</returns>
        public Task<string> GetCompetitionsAsync(string sessionid)
        {
            #region body
            string hash = String.Empty;

            if (!String.IsNullOrEmpty(sessionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, String.Format("/competities"), sessionid)).ToLower();
            }

            string requesturi = String.Format("http://api.knvbdataservice.nl/api/competities?PHPSESSID={0}&hash={1}", sessionid, hash);


            using (WebClient client = CreateWebClient())
            {
                return client.DownloadStringTaskAsync(requesturi);
            }

            #endregion
        }

        /// <summary>
        /// Will retrieve a list of competitions
        /// </summary>
        /// <param name="sessionid">The session key (can be retrieved by calling the init function)</param>
        /// <returns>A json string containing a list of competitions</returns>
        public string GetCompetitions(string sessionid)
        {
            #region body
            string hash = String.Empty;

            if (!String.IsNullOrEmpty(sessionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, String.Format("/competities"), sessionid)).ToLower();
            }

            string requesturi = String.Format("http://api.knvbdataservice.nl/api/competities?PHPSESSID={0}&hash={1}", sessionid, hash);


            using (WebClient client = CreateWebClient())
            {
                return client.DownloadString(requesturi);
            }

            #endregion
        }

        public string GetCompetitionRanking(string sessionid, string teamid, string District, string CompId, string ClassId, string PouleId, string querystring)
        {
            #region body
            string hash = String.Empty;
            string path = String.Format("/competities/{0}/{1}/{2}/{3}/{4}/ranking", teamid, District, CompId, ClassId, PouleId);
            if (!String.IsNullOrEmpty(sessionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, path, sessionid)).ToLower();
            }

            string requesturi = String.Format("http://api.knvbdataservice.nl/api/{2}?PHPSESSID={0}&hash={1}{3}", sessionid, hash, path, querystring);

            using (WebClient client = CreateWebClient())
            {
                return client.DownloadString(requesturi);

            }
            #endregion
        }

        public Task<string> GetCompetitionRankingAsync(string sessionid, string teamid, string District, string CompId, string ClassId, string PouleId, string querystring)
        {
            #region body
            string hash = String.Empty;
            string path = String.Format("/competities/{0}/{1}/{2}/{3}/{4}/ranking", teamid, District, CompId, ClassId, PouleId);
            if (!String.IsNullOrEmpty(sessionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, path, sessionid)).ToLower();
            }

            string requesturi = String.Format("http://api.knvbdataservice.nl/api/{2}?PHPSESSID={0}&hash={1}{3}", sessionid, hash, path, querystring);

            using (WebClient client = CreateWebClient())
            {
                return client.DownloadStringTaskAsync(requesturi);

            }
            #endregion
        }

        public string GetCompetitionResults(string sessionid, string teamid, string District, string CompId, string ClassId, string PouleId, string querystring)
        {
            #region body
            string hash = String.Empty;
            string path = String.Format("/competities/{0}/{1}/{2}/{3}/{4}/results", teamid, District, CompId, ClassId, PouleId);
            if (!String.IsNullOrEmpty(sessionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, path, sessionid)).ToLower();
            }

            string requesturi = String.Format("http://api.knvbdataservice.nl/api/{2}?PHPSESSID={0}&hash={1}{3}", sessionid, hash, path, querystring);

            using (WebClient client = CreateWebClient())
            {
                return client.DownloadString(requesturi);
            }
            #endregion
        }

        public Task<string> GetCompetitionResultsAsync(string sessionid, string teamid, string District, string CompId, string ClassId, string PouleId, string querystring)
        {
            #region body
            string hash = String.Empty;
            string path = String.Format("/competities/{0}/{1}/{2}/{3}/{4}/results", teamid, District, CompId, ClassId, PouleId);
            if (!String.IsNullOrEmpty(sessionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, path, sessionid, querystring)).ToLower();
            }

            string requesturi = String.Format("http://api.knvbdataservice.nl/api/{2}?PHPSESSID={0}&hash={1}{3}", sessionid, hash, path, querystring);

            using (WebClient client = CreateWebClient())
            {
                return client.DownloadStringTaskAsync(requesturi);
            }
            #endregion
        }


        public string GetCompetitionSchedule(string sessionid, string teamid, string District, string CompId, string ClassId, string PouleId, string querystring)
        {
            #region body
            string hash = String.Empty;
            string path = String.Format("/competities/{0}/{1}/{2}/{3}/{4}/schedule", teamid, District, CompId, ClassId, PouleId);
            if (!String.IsNullOrEmpty(sessionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, path, sessionid)).ToLower();
            }

            string requesturi = String.Format("http://api.knvbdataservice.nl/api/{2}?PHPSESSID={0}&hash={1}{3}", sessionid, hash, path, querystring);


            using (WebClient client = CreateWebClient())
            {
                return client.DownloadString(requesturi);
            }
            #endregion
        }

        public Task<string> GetCompetitionScheduleAsync(string sessionid, string teamid, string District, string CompId, string ClassId, string PouleId, string querystring)
        {
            #region body
            string hash = String.Empty;
            string path = String.Format("/competities/{0}/{1}/{2}/{3}/{4}/schedule", teamid, District, CompId, ClassId, PouleId);
            if (!String.IsNullOrEmpty(sessionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, path, sessionid)).ToLower();
            }

            string requesturi = String.Format("http://api.knvbdataservice.nl/api/{2}?PHPSESSID={0}&hash={1}{3}", sessionid, hash, path, querystring);

            using (WebClient client = CreateWebClient())
            {
                return client.DownloadStringTaskAsync(requesturi);
            }
            #endregion
        }
        #endregion

        #region matches calls
        /// <summary>
        /// Will retrieve a list of matches
        /// </summary>
        /// <param name="sessionid">The session key (can be retrieved by calling the init function)</param>
        /// <param name="querystring">A additional querystring (should start with a &)</param>
        /// <returns>A json string containing a list of matches</returns>
        public Task<string> GetMatchesAsync(string sessionid, string querystring)
        {
            #region body
            string hash = String.Empty;

            if (!String.IsNullOrEmpty(sessionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, String.Format("/wedstrijden"), sessionid)).ToLower();
            }

            string requesturi = String.Format("http://{0}/api/wedstrijden?PHPSESSID={1}&hash={2}{3}", ApiDomain, sessionid, hash, querystring);

            using (WebClient client = CreateWebClient())
            {
                return client.DownloadStringTaskAsync(requesturi);
            }
            #endregion
        }

        /// <summary>
        /// Will retrieve a list of matches
        /// </summary>
        /// <param name="sessionid">The session key (can be retrieved by calling the init function)</param>
        /// <param name="querystring">A additional querystring (should start with a &)</param>
        /// <returns>A json string containing a list of matches</returns>
        public string GetMatches(string sessionid, string querystring)
        {
            #region body
            string hash = String.Empty;

            if (!String.IsNullOrEmpty(sessionid))
            {
                hash = HashHelper.CalculateMD5Hash(String.Format("{0}#{1}#{2}", knvbdataservice_apisleutel, String.Format("/wedstrijden"), sessionid)).ToLower();
            }

            string requesturi = String.Format("http://{0}/api/wedstrijden?PHPSESSID={1}&hash={2}{3}", ApiDomain, sessionid, hash, querystring);

            using (WebClient client = CreateWebClient())
            {
                return client.DownloadString(requesturi);
            }
            #endregion
        }
        #endregion
    }
}
